function injectCSS (selector, declaration) {
  document.styleSheets[0].addRule(selector, declaration);
}

function freeBloomberg () {
  // remove overlay and re-enable scrolling
  let overlay = document.querySelector("#graphics-paywall-overlay");
  if (overlay) {
    overlay.remove();
  }
  document.querySelector("body").setAttribute("data-paywall-overlay-status", "");

  // remove display: null; styles
  document.querySelectorAll("p").forEach(p => p.setAttribute("style",""));
  // remove blurring from paywall-inline-tout
  injectCSS('.paywall-inline-tout:before', 'content: none !important');
}
freeBloomberg();
