function buttonClicked(tab) {
  chrome.tabs.executeScript(tab.id, {file: "free-bloomberg.js"});
};
chrome.pageAction.onClicked.addListener(buttonClicked);

function tabUpdated(tabId, changeInfo, tab) {
  if (tab.url.indexOf("bloomberg.com/news/articles/")) {
    chrome.pageAction.show(tabId);
  }
};
chrome.tabs.onUpdated.addListener(tabUpdated);
